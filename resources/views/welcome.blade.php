<div class="flex items-center justify-end mt-4">
    <a href="{{ url('login/google') }}">
        <img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png">
    </a>
</div>