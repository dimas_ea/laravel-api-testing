<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\AuthRepositoryInterface;
use App\Interfaces\UserOtpRepositoryInterface;
use App\Traits\MessageTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserAuthRequest;

class AuthController extends Controller
{
    use MessageTrait;

    private UserRepositoryInterface $userRepository;
    private AuthRepositoryInterface $authRepository;
    private UserOtpRepositoryInterface $userOtpRepository;

    public function __construct(UserRepositoryInterface $userRepository, AuthRepositoryInterface $authRepository, UserOtpRepositoryInterface $userOtpRepository){
        $this->userRepository = $userRepository;
        $this->authRepository = $authRepository;
        $this->userOtpRepository = $userOtpRepository;
    }

    public function Registration(UserAuthRequest $request){

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'phone_number' => $request->input('phone_number')
        ];

        $user = $this->userRepository->createUser($data);

        // $otp = $this->userOtpRepository->generateOtpCode($user->phone_number);
        // $user['otp_code'] = $otp;

        $generate_otp = $this->userOtpRepository->generateOtpCode();
        $this->userOtpRepository->createUserOtp($user->id, $generate_otp);

        return $this->responseMessage('success', 'OTP Token Berhasil Dibuat', $user->user_otp->otp_code);
    }

    public function login(Request $request){
        $data = [
            'condition' => 'email',
            'value' => $request->input('email')
        ];

        $user = $this->userRepository->FindUserBySingleCondition($data);

        // Checking Data User
        if(!$user || !$this->authRepository->userAuthCheck($user, $request->input('password'))){
            return $this->responseMessage('error', 'Email atau Password Salah');
        }

        // $otp = $this->userOtpRepository->generateOtpCode($user->phone_number);
        // $user['otp_code'] = $otp;
        $generate_otp = $this->userOtpRepository->generateOtpCode();
        $otp = $user->user_otp;
        if($otp){
            if(now()->gt($otp->expired_at)){
                $this->userOtpRepository->updateUserOtp($user, $generate_otp);
            }
        }else{
            $this->userOtpRepository->createUserOtp($user->id, $generate_otp);
        }

        return $this->responseMessage('success', 'OTP Token Berhasil Dibuat', $user->user_otp->otp_code);
    }

    public function verificationOtpCode(Request $request){
        $user = $this->userOtpRepository->verificationOtpCode($request->input('user_id'), $request->input('otp_code'));

        if($user){
            return $this->responseMessage('success', 'Akun Berhasil Login', $this->authRepository->userCreateToken($request->input('device_name'), $user));
        }else{
            return $this->responseMessage('error', 'Kode OTP Salah');
        };
    }

    public function getUserData(Request $request){
        if(Auth('sanctum')->check()){
            return $this->responseMessage('success', 'Berhasil Mendapatkan Data User', Auth::guard('sanctum')->user());
        }else{
            return $this->responseMessage('error', 'Data User Tidak Ditemukan');
        }
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return $this->responseMessage('success', 'Akun Berhasil Logout');
    }

    public function generateNewOtpCode(Request $request){
        $data = [
            'condition' => 'id',
            'value' => $request->input('user_id')
        ];

        $user = $this->userRepository->FindUserBySingleCondition($data);

        // $otp = $this->userOtpRepository->generateOtpCode($request->input('phone_number'));

        $generate_otp = $this->userOtpRepository->generateOtpCode();
        $this->userOtpRepository->updateUserOtp($user, $generate_otp);

        return $this->responseMessage('success', 'OTP Token Berhasil Dibuat', $user->user_otp->otp_code);
    }
}
