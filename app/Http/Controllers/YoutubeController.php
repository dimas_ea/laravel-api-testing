<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Video;

class YoutubeController extends Controller
{
    public function index(){
        $max = 10;
        $video_url = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=". env('YOUTUBE_CHANNEL_ID') ."&maxResults=" . $max . "&order=date&type=video&creatorContentType=SHORTS&videoType=any&key=" . env('YOUTUBE_KEY');

        $url = "https://www.youtube.com/embed/";

        $result = Http::get($video_url);

        $videos = [];
        foreach($result['items'] as $items){
            $videos['url'][] = $url.$items['id']['videoId'];
        }

        return view('welcome', [
            'videos' =>$videos
        ]);
    }
}
