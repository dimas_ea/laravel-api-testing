<?php

namespace App\Interfaces;

use App\Models\User;

interface UserOtpRepositoryInterface{
    public function generateOtpCode();
    public function verificationOtpCode($user_id, $otp_code);
    public function createUserOtp($user_id, $otp_code);
    public function updateUserOtp(User $user, $otp_code);
}