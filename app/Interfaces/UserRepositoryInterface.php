<?php

namespace App\Interfaces;

interface UserRepositoryInterface{
    public function createUser($user);
    public function FindUserBySingleCondition($data);
}