<?php

namespace App\Interfaces;

use App\Models\User;

interface AuthRepositoryInterface{
    public function userCreateToken($device, User $user);
    public function userAuthCheck(User $user, $password);
}