<?php

namespace App\Traits;

trait MessageTrait{
    
    protected function responseMessage($status, $message, $data = []){
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }
}