<?php

namespace App\Repositories;

use App\Models\User;
use App\Interfaces\AuthRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class AuthRepository implements AuthRepositoryInterface{

    public function userCreateToken($device, User $user){
        return $user->createToken($device)->plainTextToken;
    }

    public function userAuthCheck(User $user, $password){
        if(!Hash::check($password, $user->password)){
            return FALSE;
        }

        return TRUE;
    }
}