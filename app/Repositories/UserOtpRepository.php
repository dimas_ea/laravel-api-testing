<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\UserOtp;
use App\Interfaces\UserOtpRepositoryInterface;

class UserOtpRepository implements UserOtpRepositoryInterface{

    // public function generateOtpCode(){
    //     $user = User::where('phone_number', $phone_number)->first();

    //     $otp = $user->user_otp;

    //     $rand_otp = rand(100000, 999999);

    //     if($otp){
    //         if(now()->gt($otp->expired_at)){
    //             $otp->update([
    //                 'otp_code' => $rand_otp,
    //                 'expired_at' => now()->addMinutes(10)
    //             ]);
    //         }
    //         return $otp->otp_code;
    //     }else{
            // UserOtp::create([
            //     'user_id' =>$user->id,
            //     'otp_code' => $rand_otp,
            //     'expired_at' => now()->addMinutes(10)
            // ]);
    //     }

    //     return $rand_otp;
    // }

    public function generateOtpCode(){
        return rand(100000, 999999);
    }

    public function createUserOtp($user_id, $otp_code){
        $userOtp = UserOtp::create([
            'user_id' => $user_id,
            'otp_code' => $otp_code,
            'expired_at' => now()->addMinutes(10)
        ]);
    }

    public function updateUserOtp(User $user, $otp_code){
        $user->user_otp->update([
            'otp_code' => $otp_code,
            'expired_at' => now()->addMinutes(10)
        ]);
    }

    public function verificationOtpCode($user_id, $otp_code){
        $user_otp = UserOtp::where([
            ['user_id', $user_id],
            ['otp_code', $otp_code]
        ])->first();

        return $user_otp ? $user_otp->user : null;
    }
}