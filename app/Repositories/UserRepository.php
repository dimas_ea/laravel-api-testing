<?php

namespace App\Repositories;

use App\Models\User;
use App\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface{

    public function createUser($user){
        return User::create($user);
    }

    public function FindUserBySingleCondition($data){
        return User::where($data['condition'], $data['value'])->first();
    }
}