<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// Interface
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\AuthRepositoryInterface;
use App\Interfaces\UserOtpRepositoryInterface;

// Repository
use App\Repositories\UserRepository;
use App\Repositories\AuthRepository;
use App\Repositories\UserOtpRepository;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);
        $this->app->bind(UserOtpRepositoryInterface::class, UserOtpRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
