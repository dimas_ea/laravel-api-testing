<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GoogleLoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});

Route::post('registration', [AuthController::class, 'registration'])->name('registration');
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('user-info', [AuthController::class, 'getUserData'])->name('info');
Route::post('otp-verification', [AuthController::class, 'verificationOtpCode'])->name('otp-verification');
Route::post('generate-new-otp', [AuthController::class, 'generateNewOtpCode'])->name('generate-new-otp');

// Google Signin
Route::get('login-google', [GoogleLoginController::class, 'redirect'])->name('google-redirect');
Route::get('login-google-callback', [GoogleLoginController::class, 'callback'])->name('google-callback');